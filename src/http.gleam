import gleam/io
import gleam/result
import gleam/dict
import gleam/bit_array

pub type HttpRequestKind {
  Get
  Post
  Head
}

pub type HttpRequest {
  HttpRequest(kind: HttpRequestKind, path: String, body: BitArray)
}

pub type HttpResponse {
  HttpResponse(code: Int, status: String, body: BitArray)
}

pub type RequestHandler {
  RequestHandler(handle_request: fn(HttpRequest) -> HttpResponse)
}

pub type ResponseWriter {
  ResponseWriter(write_response: fn(HttpResponse) -> Nil)
}

pub type Server {
  Server(request_handler: RequestHandler, response_writer: ResponseWriter)
}

pub fn path_router(routes: dict.Dict(String, RequestHandler)) -> RequestHandler {
  let handler = fn(request: HttpRequest) {
    routes
    |> dict.get(request.path)
    |> result.map(fn(handler) { handler.handle_request(request) })
    |> result.unwrap(or: HttpResponse(
      404,
      "NOT FOUND",
      bit_array.from_string("Not found"),
    ))
  }
  RequestHandler(handler)
}

pub fn text(body: String) -> RequestHandler {
  let handler = fn(_) { HttpResponse(200, "OK", bit_array.from_string(body)) }
  RequestHandler(handler)
}

pub fn dummy_response_writer() -> ResponseWriter {
  let handler = fn(response) {
    io.debug(response)
    io.println("")
  }
  ResponseWriter(handler)
}

pub fn serve(req: HttpRequest, server: Server) {
  server.response_writer.write_response(server.request_handler.handle_request(
    req,
  ))
}
