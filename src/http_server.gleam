import gleam/io
import gleam/dict
import gleam/list
import http

pub fn main() {
  let router =
    http.path_router(
      dict.new()
      |> dict.insert("/", http.text(""))
      |> dict.insert("/a", http.text("You are browsing /a"))
      |> dict.insert("/b", http.text("You are browsing /b"))
      |> dict.insert("/c", http.text("You are browsing /c"))
      |> dict.insert("/d", http.text("You are browsing /d")),
    )
  let logging_middleware =
    http.RequestHandler(handle_request: fn(request) {
      io.debug(request)
      router.handle_request(request)
    })
  let server =
    http.Server(
      request_handler: logging_middleware,
      response_writer: http.dummy_response_writer(),
    )

  [
    http.HttpRequest(path: "/xyz", kind: http.Get, body: <<>>),
    http.HttpRequest(path: "/a", kind: http.Get, body: <<>>),
    http.HttpRequest(path: "/b", kind: http.Get, body: <<>>),
    http.HttpRequest(path: "/ganda_cena", kind: http.Get, body: <<>>),
  ]
  |> list.each(fn(req) { http.serve(req, server) })
}
